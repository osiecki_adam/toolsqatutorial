﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System.Collections.ObjectModel;

namespace toolsQATutorial.Exercises.Chapter06HandlingTables
{
    class Ex09
    {
        IWebDriver driver;
        string baseURL;
        IWebElement elemTable;
        IList<IWebElement> lstTrElem;
        IList<IWebElement> elemTableTd;
        List<IWebElement> lstTdElem;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "https://en.wikipedia.org/wiki/Programming_languages_used_in_most_popular_websites";
            driver.Url = baseURL;
        }

        [Test]
        public void ExerciseTo9Chapter()
        {
            elemTable = driver.FindElement(By.XPath("//div[@id='mw-content-text']//table[1]"));
            lstTrElem = elemTable.FindElements(By.TagName("tr"));
            string strRowData="";

            foreach(var elemTr in lstTrElem)
            {
                lstTdElem = new List<IWebElement>(elemTr.FindElements(By.TagName("td")));
                if (lstTdElem.Count > 0)
                {
                    foreach(var elemTd in lstTdElem)
                    {
                        strRowData = strRowData + elemTd.Text + "\t\t";
                    }
                }
                else
                {
                    Console.WriteLine("This is Header Row");
                    Console.WriteLine(lstTrElem[0].Text.Replace(" ", "\t\t"));
                }
                Console.WriteLine(strRowData);
                strRowData = string.Empty;
            }
            Console.WriteLine("");
            driver.Quit();
        }

        [TearDown]
        public void tearDown()
        {
            driver.Quit();
        }
    }
}