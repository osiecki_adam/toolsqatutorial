﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace toolsQATutorial.Exercises.Chapter11WindowHandle
{
    class Example19
    {
        IWebDriver driver;
        string baseURL = "http://toolsqa.com/automation-practice-switch-windows/";

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Url = baseURL;
        }

        [Test]
        public void MyExample19()
        {
            Console.WriteLine("Current Window Handle: " + driver.CurrentWindowHandle);
            IWebElement newBrowserWindowBtn = driver.FindElement(By.Id("button1"));

            for(int i=0; i<3; i++)
            {
                newBrowserWindowBtn.Click();
            }

            List<string> windowList = driver.WindowHandles.ToList();

            //Display list of window handles
            Console.WriteLine("List of window handles: ");
            foreach (var handle in windowList)
            {
                Console.WriteLine(handle);
            }

            //Switch to windows
            int count = 0;
            
            foreach(var handle in windowList)
            {
                
                driver.SwitchTo().Window(windowList.ElementAt(count));
                Console.WriteLine("Current Window: " + driver.CurrentWindowHandle);
                count++;
            }
            
            //Close each windows

            count = 3;
            
            foreach (var handle in windowList)
            {
                Console.WriteLine("Closing window:      " + driver.CurrentWindowHandle);
                //Closing current window
                //After closing current window it is necessary to switch to another
                driver.Close();
                if (count > 0)
                {
                    count--;
                    driver.SwitchTo().Window(windowList.ElementAt(count));
                    Console.WriteLine("Current window is: " + driver.CurrentWindowHandle);
                }
                

            }


        }

    }

}
