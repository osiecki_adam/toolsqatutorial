﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace toolsQATutorial.Exercises.Chapter03FindElements
{
    class Ex04
    {
        IWebDriver driver;
        string baseURL;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-form/";
            driver.Url = baseURL;
        }

        [Test]
        public void fourthExampleTest()
        {
            IWebElement firstname = driver.FindElement(By.Name("firstname"));
            IWebElement lastname = driver.FindElement(By.Name("lastname"));
            IWebElement submitButton = driver.FindElement(By.Id("submit"));
            firstname.SendKeys("Andrzej");
            lastname.SendKeys("Golota");
            submitButton.Submit();
        }
    }
}
