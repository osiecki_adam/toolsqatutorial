﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace toolsQATutorial.Exercises.Chapter03FindElements
{
    class Ex05
    {
        IWebDriver driver;
        string baseURL;
        IWebElement partialElement;
        IWebElement submitButton;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-form/";
            driver.Url = baseURL;
        }

        [Test]
        public void fifthTest()
        {
            partialElement = driver.FindElement(By.PartialLinkText("Partial"));
            partialElement.Click();
            submitButton = driver.FindElement(By.TagName("button")); 
            Console.WriteLine(submitButton.TagName);
            driver.FindElement(By.LinkText("Link Test")).Click();



        }

    }
}
