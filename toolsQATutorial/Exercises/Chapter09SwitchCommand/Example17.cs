﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace toolsQATutorial.Exercises.Chapter09SwitchCommand
{
    class Example17
    {

        IWebDriver driver;
        string baseUrl = "http://toolsqa.wpengine.com/automation-practice-switch-windows/";
        IAlert alert;
        IWebDriver newWindow;
        IWebDriver originalTab;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Url = baseUrl;
        }

        [Test]
        public void MyExample17()
        {

        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }

        public void OpenJavaScriptAlert()
        {
            IWebElement alertButton = driver.FindElement(By.Id("alert"));
            alertButton.Click();
            alert = driver.SwitchTo().Alert();
            string expectedAlertText = "Knowledge increases by sharing but not by saving. Please share " +
        "this website with your friends and in your organization.";
            Assert.AreEqual(expectedAlertText, alert.Text);
        }

        [Test]
        public void OpenNewBrowserWindow()
        {
            IWebElement newBrowserWindowBtn = driver.FindElement(By.Id("button1"));
            Assert.AreEqual(1, driver.WindowHandles.Count);

            string originalWindowTitle = "Demo Windows for practicing Selenium Automation";
            Assert.AreEqual(originalWindowTitle, driver.Title);

            newBrowserWindowBtn.Click();
            Assert.AreEqual(2, driver.WindowHandles.Count);

            string newWindowHandle = driver.WindowHandles.Last();
            newWindow = driver.SwitchTo().Window(newWindowHandle);
            Console.WriteLine(newWindowHandle);

            string expectedNewWindowTitle = "QA Automation Tools Tutorial";
            Assert.AreEqual(expectedNewWindowTitle, newWindow.Title);
        }

        [Test]
        public void OpenBrowserTab()
        {
            IWebElement alertButton = driver.FindElement(By.XPath("/html/body/div[1]/div[5]/div[2]/div/div/p[4]/button"));
            alertButton.Click();

            string originalTabTitle = "Demo Windows for practicing Selenium Automation";
            Assert.AreEqual(originalTabTitle, driver.Title);

            string newTabHandle = driver.WindowHandles.Last();
            IWebDriver newTab = driver.SwitchTo().Window(newTabHandle);

            var expectedNewTabTitle = "QA Automation Tools Tutorial";
            Assert.AreEqual(expectedNewTabTitle, newTab.Title);

            originalTab = driver.SwitchTo().Window(driver.WindowHandles.First());
            Assert.AreEqual(originalTabTitle, originalTab.Title);
        }

    }
}
