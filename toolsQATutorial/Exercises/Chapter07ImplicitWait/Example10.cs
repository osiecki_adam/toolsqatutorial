﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;

namespace toolsQATutorial.Exercises.Chapter07ImplicitWait
{
    class Example10
    {
        IWebDriver driver;
        string baseURL;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://google.com";
            driver.Url = baseURL;
            
        }

        [Test]
        public void Exercise10()
        {
            //Setting implicit Wait
            //Wait for element present
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            //Setting time for page to load 
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);

            //Setting time for scrypt to asynchronous execute
            driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(10);

            //Setting sleep
            Thread.Sleep(6000);
;        }

        [TearDown]
        public void TearDown()
        {
           // driver.Quit();
        }
    }
}
