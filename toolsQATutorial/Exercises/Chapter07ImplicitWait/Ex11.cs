﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;

namespace toolsQATutorial.Exercises.Chapter07ImplicitWait
{
    class Ex11
    {
        IWebDriver driver;
        string baseURL;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-switch-windows/";
            driver.Url = baseURL;
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }


        [Test]
        public void Exercise10()
        {
            driver.FindElement(By.Id("target"));
        }

        [TearDown]
        public void TearDown()
        {
             driver.Quit();
        }
    }
}
