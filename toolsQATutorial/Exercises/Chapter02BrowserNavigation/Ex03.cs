﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace toolsQATutorial.Exercises.Chapter02BrowserNavigation
{
    class Ex03
    {
        IWebDriver driver;
        string baseURL = "http://demoqa.com/";

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Url = baseURL;
            
        }

        [Test]
        public void anotherTest()
        {

            driver.FindElement(By.CssSelector(" .menu-item-374")).Click() ;
            driver.Navigate().Back();
            driver.Navigate().Forward();
            driver.Navigate().GoToUrl(baseURL);
            driver.Navigate().Refresh();
            driver.Quit();

        }
        
    }
}
