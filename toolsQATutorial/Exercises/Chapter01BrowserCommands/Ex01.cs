﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace toolsQATutorial.Exercises.Exercise01
{

    class Ex01
    {
        IWebDriver driver;
        string baseURL;
        int baseURLlength;

        string pageTitle;
        int pageTitleLength;

        string pageSource;
        int pageSourceLength;

        [SetUp]
        public void setUp()
        {
            driver = new ChromeDriver();
        }

        [Test]
        public void driverCommands()
        {
            driver.Url = "http://DemoQA.com";
            
            //getting title and title length
            pageTitle = driver.Title;
            pageTitleLength = pageTitle.Length;
            Console.WriteLine("Page title is: " + pageTitle);
            Console.WriteLine("Page title length is: " + pageTitleLength);
            
            //getting url and url length
            baseURL = driver.Url;
            baseURLlength = baseURL.Length;
            Console.WriteLine("Url is: " + baseURL);
            Console.WriteLine("Url length is: " + baseURLlength);

            //getting page source
            pageSourceLength = driver.PageSource.Length;
            Console.WriteLine("PageSource length is: " + pageSourceLength);

        }

        [TearDown]
        public void tearDown()
        {
            driver.Quit();
        }
    }
}
