﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace toolsQATutorial.Exercises.Chapter01
{
    class Ex02
    {
        IWebDriver driver;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
        }

        [Test]
        public void secondExercise()
        {
            driver.Url = ("http://demoqa.com/frames-and-windows/");
            driver.FindElement(By.CssSelector("#tabs-1 > div > p > a")).Click(); ;
            driver.Close();
        }
    }
}
