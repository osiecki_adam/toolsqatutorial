﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace toolsQATutorial.Exercises.Chapter08AdvancedWaits
{

    //WebdriverWait example 2

    class Example13
    {
        IWebDriver driver;
        string baseURL;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-switch-windows/";
            driver.Url = baseURL;

        }

        [Test]
        public void MyExample13()
        {
            //Webdriver Wait
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            Func<IWebDriver, IWebElement> waitForElement = new Func<IWebDriver, IWebElement>((IWebDriver Web) =>
            {
                Console.WriteLine("Waiting for color to change");
                IWebElement element = Web.FindElement(By.Id("colorVar"));
                if (element.GetAttribute("style").Contains("red"))
                {
                    Console.WriteLine("Color has changed");
                    return element;
                }
                return null;
            });
            Console.WriteLine("Inner HTML of element is: "+
            wait.Until(waitForElement).GetAttribute("innerHTML"));
        }
    }
}
