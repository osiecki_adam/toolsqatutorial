﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace toolsQATutorial.Exercises.Chapter08AdvancedWaits
{
    class Ex15
    {
        IWebDriver driver;
        string baseURL;
        WebDriverWait wait;
        string clockText = "";

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-switch-windows/";
            driver.Url = baseURL;
            wait = new WebDriverWait(driver, TimeSpan.FromMinutes(2));
            
        }

        [Test]
        public void Exercise15()
        {
            Func<IWebDriver, bool> waiter = new Func<IWebDriver, bool>((IWebDriver Web) =>
             {

                 IWebElement element = Web.FindElement(By.Id("clock"));
                 clockText = element.Text;
                 if (clockText.Contains("Buzz"))
                 {
                     Console.WriteLine(clockText);
                     return true;
                 }
                 Console.WriteLine("Current time is: " + clockText);
                 return false;
             });
            wait.Until(waiter);
        }
    }
}
