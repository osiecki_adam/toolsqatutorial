﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace toolsQATutorial.Exercises.Chapter08AdvancedWaits
{
    class Example14
    {

        //DefaultWait example 3

        IWebDriver driver; 
        string baseURL;
        IWebElement element;
        DefaultWait<IWebElement> wait;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-switch-windows/";
            driver.Url = baseURL;
            element = driver.FindElement(By.Id("colorVar"));
            wait = new DefaultWait<IWebElement>(element);
            wait.Timeout = TimeSpan.FromMinutes(1);
            wait.PollingInterval = TimeSpan.FromMilliseconds(100);
        }

        [Test]
        public void MyExample14()
        {
            Func<IWebElement, bool> waiter = new Func<IWebElement, bool>((IWebElement ele) =>
            {
                String styleAttrib = element.GetAttribute("style");
                if (styleAttrib.Contains("red"))
                {
                    Console.WriteLine("Color has changed: " + styleAttrib);
                    return true;
                }
                else
                {
                    Console.WriteLine("Color is still " + styleAttrib);
                    return false;
                }
            });
            wait.Until(waiter);
        }
    }
}
