﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace toolsQATutorial.Exercises.Chapter08AdvancedWaits
{

    class Ex16
    {
        IWebDriver driver;
        string baseUrl="http://toolsqa.wpengine.com/automation-practice-switch-windows/";
        DefaultWait<IWebDriver> wait;
        string clockText = "";

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Url = baseUrl;
            wait = new DefaultWait<IWebDriver>(driver);
            wait.Timeout = TimeSpan.FromMinutes(2);
            wait.PollingInterval = TimeSpan.FromSeconds(1);              
        }

        [Test]
        public void Exercise16()
        {
            Func<IWebDriver,bool> waiter = new Func<IWebDriver, bool>((IWebDriver Web) =>
            {
                IWebElement element = Web.FindElement(By.Id("clock"));
                clockText = element.Text;

                if (element.Text.Contains("Buzz"))
                {
                    Console.WriteLine(clockText);
                    return true;
                }
                Console.WriteLine("Time on the clock: " + clockText);    
                return false;
            });

            wait.Until(waiter);
        }
        
        
    }
}
