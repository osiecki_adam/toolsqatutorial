﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace toolsQATutorial.Exercises.Chapter08AdvancedWaits
{

    //WebdriverWait example 1

    class Example12
    {
        IWebDriver driver;
        string baseURL;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-switch-windows/";
            driver.Url = baseURL;

        }

        [Test]
        public void MyExample12()
        {
            //Webdriver Wait
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            Func<IWebDriver, bool> waitForElement = new Func<IWebDriver, bool>((IWebDriver Web) =>
             {
                 Console.WriteLine("Waiting for color to change");
                 IWebElement element = Web.FindElement(By.Id("colorVar"));
                 if (element.GetAttribute("style").Contains("red"))
                 {
                     Console.WriteLine("Color has changed");
                     return true;
                 }
                 return false;
             });
            wait.Until(waitForElement);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}
