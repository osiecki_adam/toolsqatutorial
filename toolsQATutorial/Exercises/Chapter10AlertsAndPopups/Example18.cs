﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace toolsQATutorial.Exercises.Chapter10AlertsAndPopups
{
    class Example18
    {
        IWebDriver driver;
        string baseURL = "http://toolsqa.wpengine.com/handling-alerts-using-selenium-webdriver/";
        IWebElement element;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Url = baseURL;
        }

        [Test]
        public void MyNewExample18()
        {
            //Clicking on button that causes alert
            driver.FindElement(By.XPath("//*[@id='content']/p[4]/button")).Click();

            //Handling alert
            IAlert simpleAlert = driver.SwitchTo().Alert();
            
            //Accepting alert
            string simpleAlertText = simpleAlert.Text;
            Console.WriteLine("Alert displayed text: " + simpleAlertText);
            simpleAlert.Accept();

            //Dismissing alert
            driver.FindElement(By.XPath("//*[@id='content']/p[8]/button")).Click();
            simpleAlertText = simpleAlert.Text;
            Console.WriteLine("Second alert displayed text: " + simpleAlertText);
            simpleAlert.Dismiss();

            //Prompting alert
            IWebElement element = driver.FindElement(By.XPath("//*[@id='content']/p[11]/button"));

            //Creating new JavaScript Executor
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click()", element);

            IAlert promptAlert = driver.SwitchTo().Alert();
            simpleAlertText = promptAlert.Text;
            Console.WriteLine("Third alert displayed text: " + simpleAlertText);
            
            Thread.Sleep(2000);
            promptAlert.SendKeys("Accepting the alert");
            Thread.Sleep(2000);
            promptAlert.Accept();

        }

        [TearDown]
        public void TearDown()
        {
            //driver.Quit();
        }
    }
}
