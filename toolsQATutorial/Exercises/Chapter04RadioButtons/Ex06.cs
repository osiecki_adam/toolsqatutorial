﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Collections.ObjectModel;

namespace toolsQATutorial.Exercises.Chapter04RadioButtons
{
    class Ex06
    {

        IWebDriver driver;
        string baseURL;
        IWebElement radioBtnFemale;
        IWebElement radioBtnExp3years;
        ReadOnlyCollection<IWebElement> chkbxProffesion;
        int chkbxProffesionCount;
        string chkbxProffesionValue;
        IWebElement chkboxAutomationTool;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-form/";
            driver.Url = baseURL;
        }

        [Test]
        public void sixthExercise()
        {

            //1
            radioBtnFemale = driver.FindElement(By.Id("sex-1"));
            bool isRadioBtnSelected = radioBtnFemale.Selected;

            if (!isRadioBtnSelected == true)
            {
                radioBtnFemale.Click();
            }

            //2
            radioBtnExp3years = driver.FindElement(By.Id("exp-2"));
            radioBtnExp3years.Click();

            //3
            chkbxProffesion = driver.FindElements(By.Name("profession"));
            chkbxProffesionCount = chkbxProffesion.Count();

            for (int i = 0; i < chkbxProffesionCount; i++)
            {
                chkbxProffesionValue = chkbxProffesion.ElementAt(i).GetAttribute("Value");

                if (chkbxProffesionValue.Equals("Automation Tester"))
                {
                    chkbxProffesion.ElementAt(i).Click();
                    break;
                }
            }

            //4
            chkboxAutomationTool = driver.FindElement(By.CssSelector("input[value='Selenium IDE']"));
            chkboxAutomationTool.Click();
        }
    }
}
