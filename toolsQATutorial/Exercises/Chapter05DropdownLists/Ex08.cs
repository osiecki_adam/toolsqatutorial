﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;


namespace toolsQATutorial.Exercises.Chapter05
{
    class Ex08
    {
        IWebDriver driver;
        string baseUrl;
        IWebElement element;
        SelectElement oSelect;
        int oSize;
        IList<IWebElement> oOptions;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseUrl = "http://toolsqa.wpengine.com/automation-practice-form/";
            driver.Url = baseUrl;
        }

        [Test]
        public void eighthExercise()
        {
            element = driver.FindElement(By.Name("selenium_commands"));
            oSelect = new SelectElement(element);

            oSelect.SelectByIndex(0);
            oSelect.SelectByIndex(0);

            oSelect.SelectByText("Navigation Commands");
            oSelect.DeselectByText("Navigation Commands");

            oOptions = oSelect.Options;
            oSize = oOptions.Count();

            for (int i=0; i< oSize; i++)
            {
                Console.WriteLine(oOptions.ElementAt(i));
                oSelect.SelectByIndex(i);
            }

            oSelect.DeselectAll();
        }
    }
}
