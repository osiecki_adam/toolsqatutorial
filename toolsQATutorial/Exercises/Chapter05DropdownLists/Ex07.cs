﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using System.Threading;

namespace toolsQATutorial.Exercises.Chapter05
{
    class Ex07
    {
        IWebDriver driver;
        string baseURL;
        IWebElement continentsDropdown;
        SelectElement oSelect;
        IList<IWebElement> elementCount;
        int elementCountSize;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            baseURL = "http://toolsqa.wpengine.com/automation-practice-form/";
            driver.Url = baseURL;
        }          

        [Test]
        public void seventhExercise()
        {
            continentsDropdown = driver.FindElement(By.Id("continents"));
            oSelect = new SelectElement(continentsDropdown);

            oSelect.SelectByIndex(1);
            Thread.Sleep(2000);
            oSelect.SelectByText("Africa");

            elementCount = oSelect.Options;
            elementCountSize = elementCount.Count();

            for (int i = 0; i<elementCountSize; i++)
            {
                Console.WriteLine(elementCount.ElementAt(i).Text);
                if (elementCount.ElementAt(i).Text.Equals("Antartica"))
                {
                    oSelect.SelectByIndex(i);
                }
            }
        }
    }
}
