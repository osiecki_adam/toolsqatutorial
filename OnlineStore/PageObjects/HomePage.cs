﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace OnlineStore.PageObjects
{
    class HomePage
    {
        //Elements
              
        [FindsBy(How = How.Id, Using = "account")]
        [CacheLookup]
        private IWebElement MyAccount { get; set; }

        //Methods

        public void ClickOnMyAccount()
        {
            MyAccount.Click();
        }
    }
}
