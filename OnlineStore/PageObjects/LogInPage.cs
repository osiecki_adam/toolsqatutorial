﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OnlineStore.TestDataAccess;
using OnlineStore.Extensions;

namespace OnlineStore.PageObjects
{
    class LogInPage
    {
        //Elements

        [FindsBy(How = How.Id, Using = "log")]
        [CacheLookup]
        private IWebElement UserName { get; set; }

        [FindsBy(How = How.Id, Using = "pwd")]
        [CacheLookup]
        private IWebElement Password { get; set; }

        [FindsBy(How = How.Id, Using = "login")]
        [CacheLookup]
        private IWebElement Submit { get; set; }

        [FindsBy(How = How.Id, Using = "account")]
        [CacheLookup]
        private IWebElement MyAccount { get; set; }
        
        //Methods
    
        public void logInToApplication(string testName)
        {
            var userData = ExcelDataAccess.GetTestData(testName);
            UserName.EnterText(userData.Username, "UserName");
            Password.EnterText(userData.Password, "Password");
            Submit.Submit();
        }    
    }
}
