﻿using OnlineStore.WrapperFactory;
using OpenQA.Selenium.Support.PageObjects;

namespace OnlineStore.PageObjects
{
    class Page
    {
        private static T GetPage<T>() where T : new()
        {
        var page = new T();
            PageFactory.InitElements(BrowserFactory.Driver, page);
        return page;
        }

        public static HomePage Home
        {
            get { return GetPage<HomePage>(); }
        }

        public static LogInPage LogIn
        {
            get { return GetPage<LogInPage>(); }
        }
    }
}
