﻿using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using Dapper;


namespace OnlineStore.TestDataAccess
{
    class ExcelDataAccess
    {
        public static string TestDataFileConnection()
        {
            var fileName = ConfigurationManager.AppSettings["TestDataSheetPath"];

           return @"Provider=Microsoft.Jet.OLEDB.4.0;" +  
           @"Data Source=" + fileName + ";" +
           @"Extended Properties="  +
           @"Excel 8.0" + ";";
        }

        public static UserData GetTestData(string keyName)
        {
            using (var connection = new OleDbConnection(TestDataFileConnection()))
            {
                connection.Open();
                var query = string.Format("select * from [dataset$] where key='{0}'", keyName);
                var value = connection.Query<UserData>(query).FirstOrDefault();
                connection.Close();
                return value;
            }
        }
    }
}
