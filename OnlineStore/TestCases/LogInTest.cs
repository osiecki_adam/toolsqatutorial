﻿
using NUnit.Framework;
using OnlineStore.PageObjects;
using OnlineStore.WrapperFactory;
using System.Configuration;

namespace OnlineStore.TestCases
{
    class LogInTest
    {
        [Test]
        public void Test()
        { 
            BrowserFactory.InitBrowser(ConfigurationManager.AppSettings["Browser"]);
            BrowserFactory.LoadApplication(ConfigurationManager.AppSettings["URL"]);
            Page.Home.ClickOnMyAccount();
            Page.LogIn.logInToApplication("LogInTest");
            BrowserFactory.CloseAllDrivers();
        }
    }
}